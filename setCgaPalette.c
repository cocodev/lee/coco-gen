#include "coco-gen_private.h"

void setCgaPalette()
{
    setPalette(0,  0b000000);
    setPalette(1,  0b001000);
    setPalette(2,  0b010000);
    setPalette(3,  0b011000);
    setPalette(4,  0b100000);
    setPalette(5,  0b101000);
    setPalette(6,  0b100010);
    setPalette(7,  0b111000);
    setPalette(8,  0b000111);
    setPalette(9,  0b001011);
    setPalette(10, 0b010111);
    setPalette(11, 0b011011);
    setPalette(12, 0b100100);
    setPalette(13, 0b101101);
    setPalette(14, 0b110110);
    setPalette(15, 0b111111);
    return;
}
