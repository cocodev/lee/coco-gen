#include "coco-gen_private.h"

void clearGraphicsScreen()
{
    for (byte physicalBlock = _PHYSICAL_60000_61FFF; physicalBlock <= _PHYSICAL_60000_61FFF + 4; physicalBlock++)
    {
        mapPhysicalToLogicalMemory(physicalBlock, _LOGICAL_2000_3FFF);
        memset(0x2000, 0b00000000, 0x2000);
    }
    mapPhysicalToLogicalMemory(_PHYSICAL_72000_73FFF, _LOGICAL_2000_3FFF);
}
