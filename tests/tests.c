#include "tests.h"

word numberOfTestsThatFailed;

BOOL AssertEqual(word value, word expected, const char *errorMessage, char *nameOfTestFunction)
{
    if (value != expected)
    {
        printf("** %s failed.\n", nameOfTestFunction);
        if (errorMessage)
        {
            printf("   %s\n", errorMessage);
        }
        printf("   Expected 0x%02X, was 0x%02X\n", expected, value);
        numberOfTestsThatFailed++;
        return FALSE;
    }
    
    return TRUE;
}

void displayTestStartAndEnd() {
    char *s, *e;
    asm
    {
        leax    program_start,pcr
        stx     :s
        leax    program_end,pcr
        stx     :e
    }
    printf("** Tests loaded at %p - %p.\n\n", s, e);
}

#define TEST(testName, testbody) void testName() {testbody}
#include "hiresPutCharTests.ctest"
#include "setPixelTests.ctest"
#undef TEST
#define TEST(testName, testbody) testName,

void (*testFunctions[])() = {
#include "hiresPutCharTests.ctest"
#include "setPixelTests.ctest"
};

int main()
{
    initCoCoSupport();
    setHighSpeed(TRUE);
    printf("\n");
    displayTestStartAndEnd();
    numberOfTestsThatFailed = 0;
    word numberOfTests = sizeof(testFunctions)/sizeof(testFunctions[0]);
    for(word index = 0; index < numberOfTests; index++)
    {
        testFunctions[index]();
    }

    if (numberOfTestsThatFailed == 0)
    {
        printf("** Passed!\n");
    }
    printf("** %d tests completed.\n", numberOfTests);
    return 0;
}
