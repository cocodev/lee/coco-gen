#ifndef _TESTS_H
#define _TESTS_H

#include <coco.h>
#include "../coco-gen.h"

#define NAMEOF(identifier) #identifier
#define ASSERTEQUAL(value, expected, errorMessage) AssertEqual(value, expected, errorMessage, __func__)

BOOL AssertEqual(word value, word expected, const char *errorMessage, char *nameOfTestFunction);

#endif
