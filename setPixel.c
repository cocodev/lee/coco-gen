#include "coco-gen_private.h"

static const byte colorMasks[] = {0b00000001, 0b00000011, 0, 0b00001111};
static const byte colorSpreadMultipliers[] = {0xFF, 0x55, 0, 0x11};
asm byte getColorFilledByte(byte color, byte bitsPerPixel)
{
    // color        = 3,S
    // bitsPerPixel = 5,S
    asm
    {
        PSHS    X,A
        LDB     6,S                     // Get color
        LDA     8,S                     // Get bitsPerPerPixel (should only be 1, 2, or 4)
        SUBA    #$01                    // Convert 1-4 TO 0-3
        LEAX    colorMasks              // Point to the table of masks
        ANDB    A,X                     // Keep only the bits from color we need
        LEAX    colorSpreadMultipliers  // Point to the table of multipliers
        LDA     A,X                     // Get the appropriate multiplier for bitsPerPixel
        MUL                             // Spread the color across the entire byte
        PULS    A,X
    }
}

static byte rightShifts[4] = {3, 2, 2, 1}; // 1 == /2, 2 == /4, 3 == /8
static byte significantBitMasks[4] = {7, 3, 3, 1};
static byte bit1Masks[8] = { 0x80, 0x40, 0x20, 0x10, 0x08, 0x04, 0x02, 0x01 };
static byte bit2Masks[4] = { 0xC0, 0x30, 0x0C, 0x03 };
static byte bit4Masks[2] = { 0xF0, 0x0F };
static byte *bitMasksArrays[] = { bit1Masks, bit2Masks, bit2Masks, bit4Masks };
BankedOffset getPixelBankedOffset(word x, word y, word numberOfBytesInRow, byte bitsPerPixel)
{
    byte bitsPerPixelIndex = bitsPerPixel - 1;
    word offset = (x >> rightShifts[bitsPerPixelIndex]) + y * numberOfBytesInRow;

    byte bank = (byte)(offset / 0x2000);
    offset = offset % 0x2000;
    byte *bitMasks = bitMasksArrays[bitsPerPixelIndex];
    byte significantMask = significantBitMasks[bitsPerPixelIndex];
    byte significantBits = (byte)(x & significantMask);
    byte pixelMask = bitMasks[significantBits];

    BankedOffset bankedOffset = { bank, offset, pixelMask };
    return bankedOffset;
}

void setPixel(word x, word y, byte color) {
    byte bpp = _videoModes[_videoModeIndex].BPP;
    word colorFilledByte = getColorFilledByte(color, bpp);
    BankedOffset bankedOffset = getPixelBankedOffset(x, y, _videoModes[_videoModeIndex].BufferWidth, bpp);

    mapPhysicalToLogicalMemory(_PHYSICAL_60000_61FFF + bankedOffset.bank, _LOGICAL_2000_3FFF);
    byte clearedPixelByte = peek(0x2000 + bankedOffset.offsetInBank) & (~bankedOffset.mask);
    poke(0x2000 + bankedOffset.offsetInBank, clearedPixelByte | (colorFilledByte & bankedOffset.mask));
}
