#include "coco-gen_private.h"

void hiresMoveCursor(byte x, byte y)
{
    struct VideoModeGime videoMode = _videoModes[_videoModeIndex];

    hiresText_CurrentCursorX = x;
    hiresText_CurrentCursorY = y;
    hiresText_NextCharacterAddress = 0x2000 + y * videoMode.BufferWidth + x * 2;
}
