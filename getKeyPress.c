#include "coco-gen_private.h"

byte getKeyPress(BOOL showCursor)
{
    byte key = waitkey(showCursor);
    if (key == 3)
    {
        exit(0);
    }
    return key;
}
