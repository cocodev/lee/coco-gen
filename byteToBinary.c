#include "coco-gen_private.h"

// buffer must be numberOfBits + 1 bytes long to hold the numberOfBits 1 or 0 characters plus the \0.
char *byteToBinary(byte value, byte numberOfBits, char *buffer)
{
    byte currentBit = (byte)(1 << (numberOfBits - 1));
    byte index = 0;
    while (currentBit != 0)
    {
        buffer[index++] = (value & currentBit) > 0 ? '1' : '0';
        currentBit >>= 1;
    }
    buffer[index] = '\0';
    return buffer;
}
