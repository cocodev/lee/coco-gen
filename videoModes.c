#include "coco-gen_private.h"

// BPL  - Bit Plane     (0=Text, 1=Graphics)
// LPR  - Lines per row (000=1, 001=1, 010=2, 011=8, 100=9, 101=10, 110=11, 111=all)
// VRES - Vertical Res  (00=192/24, 01=200/25, 10=Not Used, 11=225/28)
// HRES - Graphics      (000=16 bytes, 001=20b, 010=32b, 011=40b, 100=64b, 101=80b, 110=128b, 111=160b)
// CRES - Graphics      (00=2 colors(8 pixels/byte), 01=4c(4p/b), 10=16c(2p/b), 11=Not Used (would have been 256c!))

struct VideoModeGime _videoModes[] = {
 //                                                                                Buffer
 //    Name                     BitPlane          LPR    VRES  HRES   CRES  BPP  Width Height
 // { "Text 80x1(12 Repeat)",   BitPlaneText,     0b111, 0b00, 0b101, 0b01,   0, 80*2,    1 },
 // { "Text 80x1(13 R, -1 SL)", BitPlaneText,     0b111, 0b01, 0b101, 0b01,   0, 80*2,    1 },
 // { "Text 80x1(14 R, +1 SL)", BitPlaneText,     0b111, 0b11, 0b101, 0b01,   0, 80*2,    1 },
 // { "Text 80x17(+5 SL)",      BitPlaneText,     0b110, 0b00, 0b101, 0b01,   0, 80*2,   18 },
 // { "Text 80x18(+1 SL)",      BitPlaneText,     0b110, 0b01, 0b101, 0b01,   0, 80*2,   19 },
 // { "Text 80x19(+2 SL)",      BitPlaneText,     0b101, 0b00, 0b101, 0b01,   0, 80*2,   20 },
 // { "Text 80x20(+5 SL)",      BitPlaneText,     0b110, 0b11, 0b101, 0b01,   0, 80*2,   21 },
    { "Text 80x20",             BitPlaneText,     0b101, 0b01, 0b101, 0b01,   0, 80*2,   20 },
 // { "Text 80x21(+3 SL)",      BitPlaneText,     0b100, 0b00, 0b101, 0b01,   0, 80*2,   22 },
 // { "Text 80x22(+1 SL)",      BitPlaneText,     0b100, 0b01, 0b101, 0b01,   0, 80*2,   23 },
 // { "Text 80x22(+5 SL)",      BitPlaneText,     0b101, 0b11, 0b101, 0b01,   0, 80*2,   23 },
    { "Text 80x24",             BitPlaneText,     0b011, 0b00, 0b101, 0b01,   0, 80*2,   24 },
 // { "Text 80x25(-1 SL)",      BitPlaneText,     0b011, 0b01, 0b101, 0b01,   0, 80*2,   25 },
    { "Text 80x25",             BitPlaneText,     0b100, 0b11, 0b101, 0b01,   0, 80*2,   25 },
 // { "Text 80x28(+1 SL)",      BitPlaneText,     0b011, 0b11, 0b101, 0b01,   0, 80*2,   29 },

    { "Graphics 128x192x2",     BitPlaneGraphics, 0b001, 0b00, 0b000, 0b00,   1, 128/8,  192 },
    { "Graphics 128x200x2",     BitPlaneGraphics, 0b001, 0b01, 0b000, 0b00,   1, 128/8,  200 },
    { "Graphics 128x225x2",     BitPlaneGraphics, 0b001, 0b11, 0b000, 0b00,   1, 128/8,  225 },
    { "Graphics 128x192x4",     BitPlaneGraphics, 0b001, 0b00, 0b010, 0b01,   2, 128/4,  192 },
    { "Graphics 128x200x4",     BitPlaneGraphics, 0b001, 0b01, 0b010, 0b01,   2, 128/4,  200 },
    { "Graphics 128x225x4",     BitPlaneGraphics, 0b001, 0b11, 0b010, 0b01,   2, 128/4,  225 },
    { "Graphics 128x192x16",    BitPlaneGraphics, 0b001, 0b00, 0b100, 0b10,   4, 128/2,  192 },
    { "Graphics 128x200x16",    BitPlaneGraphics, 0b001, 0b01, 0b100, 0b10,   4, 128/2,  200 },
    { "Graphics 128x225x16",    BitPlaneGraphics, 0b001, 0b11, 0b100, 0b10,   4, 128/2,  225 },

    { "Graphics 160x192x2",     BitPlaneGraphics, 0b001, 0b00, 0b001, 0b00,   1, 160/8,  192 },
    { "Graphics 160x200x2",     BitPlaneGraphics, 0b001, 0b01, 0b001, 0b00,   1, 160/8,  200 },
    { "Graphics 160x225x2",     BitPlaneGraphics, 0b001, 0b11, 0b001, 0b00,   1, 160/8,  225 },
    { "Graphics 160x192x4",     BitPlaneGraphics, 0b001, 0b00, 0b011, 0b01,   2, 160/4,  192 },
    { "Graphics 160x200x4",     BitPlaneGraphics, 0b001, 0b01, 0b011, 0b01,   2, 160/4,  200 },
    { "Graphics 160x225x4",     BitPlaneGraphics, 0b001, 0b11, 0b011, 0b01,   2, 160/4,  225 },
    { "Graphics 160x192x16",    BitPlaneGraphics, 0b001, 0b00, 0b101, 0b10,   4, 160/2,  192 },
    { "Graphics 160x200x16",    BitPlaneGraphics, 0b001, 0b01, 0b101, 0b10,   4, 160/2,  200 },
    { "Graphics 160x225x16",    BitPlaneGraphics, 0b001, 0b11, 0b101, 0b10,   4, 160/2,  225 },

    { "Graphics 320x192x4",     BitPlaneGraphics, 0b001, 0b00, 0b101, 0b01,   2, 320/4,  192 },
    { "Graphics 320x200x4",     BitPlaneGraphics, 0b001, 0b01, 0b101, 0b01,   2, 320/4,  200 },
    { "Graphics 320x225x4",     BitPlaneGraphics, 0b001, 0b11, 0b101, 0b01,   2, 320/4,  225 },
    { "Graphics 320x192x16",    BitPlaneGraphics, 0b001, 0b00, 0b111, 0b10,   4, 320/2,  192 },
    { "Graphics 320x200x16",    BitPlaneGraphics, 0b001, 0b01, 0b111, 0b10,   4, 320/2,  200 },
    { "Graphics 320x225x16",    BitPlaneGraphics, 0b001, 0b11, 0b111, 0b10,   4, 320/2,  225 },

    { "Graphics 640x192x2",     BitPlaneGraphics, 0b001, 0b00, 0b101, 0b00,   2, 640/8,  192 },
    { "Graphics 640x200x2",     BitPlaneGraphics, 0b001, 0b01, 0b101, 0b00,   2, 640/8,  200 },
    { "Graphics 640x225x2",     BitPlaneGraphics, 0b001, 0b11, 0b101, 0b00,   2, 640/8,  225 },
    { "Graphics 640x192x4",     BitPlaneGraphics, 0b001, 0b00, 0b111, 0b01,   4, 640/4,  192 },
    { "Graphics 640x200x4",     BitPlaneGraphics, 0b001, 0b01, 0b111, 0b01,   4, 640/4,  200 },
    { "Graphics 640x225x4",     BitPlaneGraphics, 0b001, 0b11, 0b111, 0b01,   4, 640/4,  225 }
};
word NumberOfVideoModes = sizeof(_videoModes)/sizeof(struct VideoModeGime);
