#include "coco-gen_private.h"

byte _videoModeIndex;

void setVideoMode(byte videoModeIndex)
{
    _videoModeIndex = videoModeIndex;
    struct VideoModeGime videoMode = _videoModes[videoModeIndex];
    byte videoModeValue = (videoMode.BitPlane << 7)|videoMode.LPR;
    byte videoResolutionValue = (videoMode.VRES << 5) | (videoMode.HRES << 2) | videoMode.CRES;
    
    poke(0xFF98, videoModeValue);       // VideoMode        - BP(7), Unused(6), DESCEN(5), MOCH(4), H50(3), LPR2-LPR0=(2-0)
    poke(0xFF99, videoResolutionValue); // VideoResolution  - Unused(7), VRES1-VRES0(6-5), HRES2-HRES0(4-2), CRES1-CRES0(1-0)
    poke(0xFF9A, 0b00001001);           // BorderColor      - Unused=(7-6), R1(5), G1(4), B1(3), R0(2), G0(1), B0(0)
    poke(0xFF9C, 0b00000000);           // VerticalScroll   - Unused=(7-4), SCEN(3), SC2-SC0(2-0)
    if (videoMode.BitPlane == 1)
    {
        poke(0xFF9D, 0b11000000);           // VerticalOffset1  - Physical address bits 18-3=0b1100000000000000 (bits 2-0 always 0) == 0x60000
    }
    else
    {
        poke(0xFF9D, 0b11011000);           // VerticalOffset1  - Physical address bits 18-3=0b1101100000000000 (bits 2-0 always 0) == 0x6C000
    }
    poke(0xFF9E, 0b00000000);           // VerticalOffset0  - 
    poke(0xFF9F, 0b00000000);           // HorizontalOffset - HE(6), X6-X0(6-0)
    poke(0xFF90, 0b01000100);           // Init0            - CoCo Bit(7), MMU(6), IEN(5), FEN(4), M3C(3), MC2(2), MC1-MC0(1-0)
}

//                      Dedicated
// GIME Register        Address     Description
// -------------------- ----------- -----------
// initO                FF90        Bit 7       CoCo Bit - 1=Color Computer 1/2 Compatible, 0=CoCo3
//                                  Bit 6       MMU - 1=MMU enabled
//                                  Bit 5       IEN - 1=GIME IRQ output enabled to CPU, 0=disabled
//                                  Bit 4       FEN - 1=GIME FIRQ output enabled to CPU, 0=disabled
//                                  Bit 3       MC3 - 1=Vector RAM at FEXX enabled, 0=disabled
//                                  Bit 2       MC2 - 1=Standard SCS (DISK) (0=expand 1=normal)
//                                  Bit 1-0     MC1/MC0 - ROM Map Byte 1 - 0x=16k Int/16k Ext, 10=32k Int, 11=32k Ext (except interrupt vectors)
//                                  
// Video Mode           FF98        Bit 7       BP - 0 = text, 1 = Bit Plane (graphics) mode
//                                  Bit 6       Not Used
//                                  Bit 5       DESCEN - Artifact color swap (Normally 0, set to 1 if F1 held during boot).  Affects PMODE 4 artifact colors.
//                                  Bit 4       MOCH - Monochrome composite video - 1=Monochrome, 0=Color
//                                  Bit 3       H50 - 1=50hz, 0=60Hz
//                                  Bit 2-0     LPR2-LPR0 - Lines per row (000=1, 001=1, 010=2, 011=8, 100=9, 101=10, 110=11, 111=all)
//                                  
// Video Resolution     FF99        Bit 7       Not Used
//                                  Bit 6-5     VRES1-VRES0 - 00=192/24, 01=200/25, 10=Not Used, 11=225/28
//                                  Bit 4-2     HRES2-HRES0 - Graphics: 000=16 bytes, 001=20b, 010=32b, 011=40b, 100=64b, 101=80b, 110=128b, 111=160b
//                                                                Text: 0x0=32 characters(64 bytes), 0x1=40c(80b), 1x0=64c(128b), 1x1=80c(160b)
//                                  Bit 1-0     CRES1-CRES0 - Graphics: 00=2 colors(8 pixels/byte, 1 bit/pixel), 01=4c(4p/b,2b/p), 10=16c(2p/b,4b/p), 11=Not Used (would have been 256c!)
//                                                                Text: x0=No color attributes, x1=Color attribute enabled
//                                  
// Border Color         FF9A        Bit 7-6     Not Used
//                                  Bit 5-0     000000 - 111111 (0-63) - color of border - Bits 5/2=Red, Bits 4/1=Green, Bits 3/0=Blue
//                                  
// Vertical Scroll      FF9C        Bit 7-4     Not Used
//                                  Bit 3       SCEN - 1=Enabled, 0=Disabled
//                                  Bit 2-0     SC2-SC0 - 000-111 (0-7) - # of scanlines to move text screen up.
//                                  
// Vertical Offset1     FF9D        Bit 7-0     Bits 18-3 of physical address of start of Video RAM (* 8 (or << 3) == physical adress)
// Vertical OffsetO     FF9E        Bit 7-0     "    "
//                                  
// Horizontal Offset    FF9F        Bit 7       HE - Horizontal virtual screen enable (256 bytes per row)
//                                  Bit 6-0     X6-X0 - 0000000-1111111 (0-127) byte offset within the 256 byte row