#ifndef _COCO_GEN_H_
#define _COCO_GEN_H_

#include <coco.h>

#define _PHYSICAL_00000_01FFF 0x00
#define _PHYSICAL_02000_03FFF 0x01
#define _PHYSICAL_04000_05FFF 0x02
#define _PHYSICAL_06000_07FFF 0x03
#define _PHYSICAL_08000_09FFF 0x04
#define _PHYSICAL_0A000_0BFFF 0x05
#define _PHYSICAL_0C000_0DFFF 0x06
#define _PHYSICAL_0E000_0FFFF 0x07
#define _PHYSICAL_10000_01FFF 0x08
#define _PHYSICAL_12000_13FFF 0x09
#define _PHYSICAL_14000_15FFF 0x0A
#define _PHYSICAL_16000_17FFF 0x0B
#define _PHYSICAL_18000_19FFF 0x0C

#define _PHYSICAL_60000_61FFF 0x30
#define _PHYSICAL_62000_63FFF 0x31
#define _PHYSICAL_64000_65FFF 0x32
#define _PHYSICAL_66000_67FFF 0x33
#define _PHYSICAL_68000_69FFF 0x34
#define _PHYSICAL_6A000_6BFFF 0x35
#define _PHYSICAL_6C000_6DFFF 0x36
#define _PHYSICAL_6E000_6FFFF 0x37
#define _PHYSICAL_70000_71FFF 0x38
#define _PHYSICAL_72000_73FFF 0x39
#define _PHYSICAL_74000_75FFF 0x3A
#define _PHYSICAL_76000_77FFF 0x3B
#define _PHYSICAL_78000_79FFF 0x3C
#define _PHYSICAL_7A000_7BFFF 0x3D
#define _PHYSICAL_7C000_7DFFF 0x3E
#define _PHYSICAL_7E000_7FFFF 0x3F

#define _MMU_BANK_REGISTERS_FIRST_  0xFFA0
#define _LOGICAL_0000_1FFF 0
#define _LOGICAL_2000_3FFF 1
#define _LOGICAL_4000_5FFF 2
#define _LOGICAL_6000_7FFF 3
#define _LOGICAL_8000_9FFF 4
#define _LOGICAL_A000_BFFF 5
#define _LOGICAL_C000_DFFF 6
#define _LOGICAL_E000_FFFF 7

#define mapPhysicalToLogicalMemory(physical, logical) \
    {poke(_MMU_BANK_REGISTERS_FIRST_ + logical, physical); MMU[logical] = physical;}

#define poke(address, byteToStore) *(byte *)(address) = (byte)(byteToStore)
#define pokeWord(address, wordToStore) *(word *)(address) = (word)(wordToStore)
#define peek(address) *(byte *)(address)
#define peekWord(address) *(word *)(address)

#define getPalette(paletteNumber) *(byte *)(0xFFB0 + paletteNumber)
#define setPalette(paletteNumber, color) *(byte *)(0xFFB0 + paletteNumber) = (byte)(color)
#define setBorderToPalette(paletteNumber) *(byte *)(0xFF9A) = (byte)(paletteNumber)

#define ModeText80x20          0
#define ModeText80x24          1
#define ModeText80x25          2

#define ModeFirstText          ModeText80x20
#define ModeLastText           ModeText80x25

#define ModeGraphics128x192x2  3
#define ModeGraphics128x200x2  4
#define ModeGraphics128x225x2  5
#define ModeGraphics128x192x4  6
#define ModeGraphics128x200x4  7
#define ModeGraphics128x225x4  8
#define ModeGraphics128x192x16 9
#define ModeGraphics128x200x16 10
#define ModeGraphics128x225x16 11

#define ModeGraphics160x192x2  12
#define ModeGraphics160x200x2  13
#define ModeGraphics160x225x2  14
#define ModeGraphics160x192x4  15
#define ModeGraphics160x200x4  16
#define ModeGraphics160x225x4  17
#define ModeGraphics160x192x16 18
#define ModeGraphics160x200x16 19
#define ModeGraphics160x225x16 20

#define ModeGraphics320x192x4  21
#define ModeGraphics320x200x4  22
#define ModeGraphics320x225x4  23
#define ModeGraphics320x192x16 24
#define ModeGraphics320x200x16 25
#define ModeGraphics320x225x16 26

#define ModeGraphics640x192x2  27
#define ModeGraphics640x200x2  28
#define ModeGraphics640x225x2  29

#define ModeGraphics640x192x4  30
#define ModeGraphics640x200x4  31
#define ModeGraphics640x225x4  32

#define ModeFirstGraphics      ModeGraphics128x192x2
#define ModeLastGraphics       ModeGraphics640x225x4

#define BitPlaneText 0
#define BitPlaneGraphics 1

typedef struct
{
    byte bank;
    word offsetInBank;
    byte mask;
} BankedOffset;

struct VideoModeGime {
    const char *Name;
    byte BitPlane;
    byte LPR;
    byte VRES;
    byte HRES;
    byte CRES;
    byte BPP;
    byte BufferWidth;
    byte BufferHeight;
};

extern word hiresText_NextCharacterAddress;
extern byte hiresText_CurrentCursorX;
extern byte hiresText_CurrentCursorY;

extern word NumberOfVideoModes;
extern asm byte getColorFilledByte(byte color, byte bitsPerPixel);
extern BankedOffset getPixelBankedOffset(word x, word y, word numberOfBytesInRow, byte bitsPerPixel);
extern byte MMU[8];

char *byteToBinary(byte value, byte numberOfBits, char *buffer);
byte getKeyPress(BOOL showCursor);
void hiresMoveCursor(byte x, byte y);
void hiresPutChar();
void memsetWord(word *address, word wordToStore, word numberOfWords);
void setCoCo12Width32();
void savePalette(byte *paletteBuffer);
void restorePalette(byte *paletteBuffer);
void setCgaPalette();
struct VideoModeGime getVideoModeDefinition(byte videoModeIndex);
void setVideoMode(byte videoModeIndex);
void clearGraphicsScreen();
void clearHiresTextScreen();
asm byte getColorFilledByte(byte color, byte bitsPerPixel);
BankedOffset getPixelBankedOffset(word x, word y, word numberOfBytesInRow, byte bitsPerPixel);
void setPixel(word x, word y, byte color);

#endif
