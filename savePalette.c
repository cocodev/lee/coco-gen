#include "coco-gen_private.h"

void savePalette(byte *paletteBuffer)
{
    byte paletteNumber = 15;
    do
    {
        paletteBuffer[paletteNumber] = getPalette(paletteNumber);
    } while (paletteNumber--);
}
