#include "coco-gen_private.h"

void restorePalette(byte *paletteBuffer)
{
    byte paletteNumber = 15;
    do
    {
        setPalette(paletteNumber, paletteBuffer[paletteNumber]);
    } while (paletteNumber--);
}
