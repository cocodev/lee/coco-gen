#include "coco-gen_private.h"

void clearHiresTextScreen()
{
    struct VideoModeGime videoMode = _videoModes[_videoModeIndex];
    byte *address = 0x2000;
    mapPhysicalToLogicalMemory(_PHYSICAL_6C000_6DFFF, _LOGICAL_2000_3FFF);
    word bytesToClear = (word)videoMode.BufferWidth * (word)videoMode.BufferHeight;
    for(word clearCount = 0; clearCount < bytesToClear; clearCount += 2)
    {
        poke(address++, 0x20);
        poke(address++, 0x38);
    }
    mapPhysicalToLogicalMemory(_PHYSICAL_72000_73FFF, _LOGICAL_2000_3FFF);
    hiresMoveCursor(0, 0);
}
