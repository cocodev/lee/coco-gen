#include "coco-gen_private.h"

void memsetWord(word *address, word wordToStore, word numberOfWords)
{
    asm
    {
    pshs    u
    tfr     s,u
    leas    -2,s    end address

    ldd     4,u     start address (address)
    tfr     d,x
    addd    8,u     add number of words (numberOfWords) to get end address
    addd    8,u     add number of words (numberOfWords) to get end address
    std     -2,u    store in local var

    ldd    6,u      word to write (wordToStore)
    bra    _memsetword_cond

_memsetword_loop
    std    ,x++

_memsetword_cond
    cmpx    -2,u            at end?
    bne     _memsetword_loop    no, continue

    ldd     4,u     return start address
    tfr     u,s
    puls    u
    }
}
