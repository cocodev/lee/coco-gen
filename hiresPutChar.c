#include "coco-gen_private.h"

word hiresText_NextCharacterAddress = 0x2000;
byte hiresText_CurrentCursorX = 0;
byte hiresText_CurrentCursorY = 0;

void hiresPutChar()
{
    char ch;
    asm
    {
        pshs    x,b
        sta     :ch
    }

    struct VideoModeGime videoMode = _videoModes[_videoModeIndex];
    byte columnWidthInCharacters = videoMode.BufferWidth >> 1;
    byte original2000 = MMU[_LOGICAL_2000_3FFF];
    mapPhysicalToLogicalMemory(_PHYSICAL_6C000_6DFFF, _LOGICAL_2000_3FFF);
    if (ch == 13)
    {
        while(hiresText_CurrentCursorX++ < columnWidthInCharacters)
        {
            poke(hiresText_NextCharacterAddress++, 0x20);
            poke(hiresText_NextCharacterAddress++, 0);
        }
        hiresText_CurrentCursorX = 0;
        hiresText_CurrentCursorY++;
    }
    else {
        poke(hiresText_NextCharacterAddress++, ch);
        poke(hiresText_NextCharacterAddress++, 0);
        hiresText_CurrentCursorX++;
        if (hiresText_CurrentCursorX >= columnWidthInCharacters)
        {
            hiresText_CurrentCursorX = 0;
            hiresText_CurrentCursorY++;
            if (hiresText_CurrentCursorY >= videoMode.BufferHeight)
            {
                hiresText_CurrentCursorY = 0;
            }
        }
    }
    mapPhysicalToLogicalMemory(_PHYSICAL_72000_73FFF, original2000);
    asm("puls", "b,x");
}
