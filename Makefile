CMOC_COMPILE_SWITCHES = --coco --compile --intermediate
CMOC_LINK_SWITCHES = --coco --intermediate --org=4000 -L/usr/local/share/cmoc/lib
CMOC_LIBRARY_SWITCHES =  -L./ -lcmoc-crt-ecb -lcmoc-std-ecb
COCO_GEN_OFILES = \
	byteToBinary.o \
	clearGraphicsScreen.o \
	clearHiresTextScreen.o \
	getKeyPress.o \
	getVideoModeDefinition.o \
	hiresMoveCursor.o \
	hiresPutChar.o \
	memSetWord.o \
	mmu.o \
	restorePalette.o \
	savePalette.o \
	setCgaPalette.o \
	setCoCo12Width32.o \
	setPixel.o \
	setVideoMode.o \
	videoModes.o

all: libcoco-gen.a tests.bin

%.o: ../%.c ../coco-gen.h ../coco-gen_private.h
	cmoc $(CMOC_COMPILE_SWITCHES) -o $@ $<

tests.o: ../tests/tests.c ../tests/tests.h ../tests/*.ctest
	cmoc $(CMOC_COMPILE_SWITCHES) -o tests.o ../tests/tests.c

libcoco-gen.a: $(COCO_GEN_OFILES)
	lwar -c libcoco-gen.a $(COCO_GEN_OFILES)

tests.bin: tests.o libcoco-gen.a
	cmoc $(CMOC_LINK_SWITCHES) -o tests.bin tests.o $(CMOC_LIBRARY_SWITCHES) -lcoco-gen

clean:
	find . -type f \( -name "*.o" -o -name "*.a" -o -name "*.bin" -o -name "*.map" -o -name "*.link" -o -name "*.lst" -o -name "*.s" \) -delete
